echo "Starting autoconfiguration script v0.1 -- written by Orwa"
"Script running from $($psscriptroot)"

# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}

Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -Force

$wlan_adapter_regex = "Name\s*:\s*(.+)"
if (!(netsh wlan show interface | sls -pattern $wlan_adapter_regex -quiet)) { 
    write-debug "ERROR: No WLAN interface detected"
    exit 1 
}

$wlan_adapter = netsh wlan show interface | sls -pattern $wlan_adapter_regex | % { $_.matches.groups[1].value }

cd $psscriptroot
ls ".\wifi-profiles\*.xml" | % { netsh wlan add profile filename="$($_.fullname)" }

echo "Waiting for you to connect to the Internet..."

$online = test-connection 8.8.8.8 -Count 1 -Quiet
while(-not $online) {

    $wifi_networks = netsh wlan show networks | Where-Object{$_ -match 'SSID\s+\d+\s+:\s+(\S+)'} | ForEach-Object{ echo $Matches[1] }
    $known_networks = netsh wlan show profiles | Where-Object{$_ -match 'Profile\s+:\s+(\S+)'} | ForEach-Object{ echo $Matches[1] }

    foreach ($network in $wifi_networks) { 
        if ($known_networks -contains $network) { 
        
            echo "Connecting to wlan (ssid=$network)..."
            netsh wlan connect name="$network" ssid="$network" interface="$($wlan_adapter)"
            Sleep 3
            break
        }
    }

    $online = test-connection 8.8.8.8 -Count 1 -Quiet
}

if (-not $online) {
    write-debug "ERROR: Failed to connect to the Internet"
    exit 1 
}

echo "You are now connected to the Internet!"

echo "We are now force Syncing your system's time"
start-service W32Time
w32tm /resync

# standalone-installer: Cisco AnyConnect
.\installers\anyconnect-win-4.8.02045-core-vpn-predeploy-k9.msi /quiet /passive /norestart 

Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

function install-chocolatey($package, $options=@()) {
    "`[32m`[42mchoco install ""$($package)"" -y --cache-location="".\cache"" $options`[0m"
    choco upgrade "$($package)" -y --cache-location="$($pwd.path)\cache" $options 
}

# Remember paramters used when upgrading
choco feature enable -n useRememberedArgumentsForUpgrades

# Install all packages required, while passing parameters (such as -x86 for Notepad++)
cat ".\packages.txt" | % {
    if (!([string]::isnullorwhitespace($_)) -and `
        !($_ -like "#*")) {
        install-chocolatey $_.split(",")[0] $_.split(",")[1]
    }
}
